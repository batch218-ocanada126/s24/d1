//console.log("Hello World");

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript

// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.)

// New features to JavaScript

// [SECTION] Exponent Operator
console.log("ES6 Updates");
console.log("-----------------");
console.log("=> Exponent Operator");

// using Math Object Methodds
const firstNum = Math.pow(8, 3);// 8*8*8
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 3;
console.log(secondNum); //8^3


// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("-----------------");
console.log("=> Template Literals");

let name = "John";

// Pre-Template
let message = "Hello " + name + "! Welcome to programming"; // Hello John Welcome to programming
console.log("Message without template literals");
console.log(message);

// String using template literals
// Uses backtincks(``) instead of ("") or ('')
message = `Hello ${name}! Welcome to programming!`;
console.log("Message with template literals");
console.log(message);

// [SECTION] Array Destructuring
// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/

console.log("-----------------");
console.log("=> Array Destructuring");

const fullName = ["juan", "Dela", "Cruz"];

// Pre-Array Desctructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! it is nice to meet you!`);
console.log("----------");

// Array Destructuring
// variable naming for array destructuring is based on the developer choice.

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(firstName);
console.log(lastName);

console.log(`Hello ${firstName} ${firstName} ${lastName}! it is nice to meet you!`);

// [SECTION] Object Destructuring
/*
	- Shortens the syntax for accessing properties form object
	- The difference with array destructuring, this should be exact property name.
	-Syntax:
		let/const {propertyNameA, propertyNameB, propertyNameC} = object;

*/


console.log("-----------")
console.log("=> Object Destructuring")

const person = {
	givenName: "Jane",
	mName: "Dela",
	surName: "Cruz"
}


//Pre-object destrcturing
console.log(person.givenName);
console.log(person.mName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.mName} ${person.surName}! its good to see you again`)
console.log("------------")


// Object Destructuring
const {givenName, mName, surName} = person;

console.log(givenName);
console.log(mName);
console.log(surName);

console.log(`Hello ${givenName} ${mName} ${surName}! its good to see you again`);

console.log("-----------")
console.log("=> Arrow Functions")

// [SECTION] Arrow Functions
/*
	-Compact alternative syntax to traditional functions
	-Useful for code snippets where creating functions will not be reused in any other portion of code.
	-This will work with "function expression"(s17.d1)

	example of funcExpression

	let funcExpression = function funcName(){
		console.log("Hello from the other side");
	}
	funcExpression();
*/
/*
	Syntax

	let/const variableName = (parameter) => {
		// code to execute;
	}
	//invocation
	variableName(argument)
*/
	
const hello = () => {
	console.log("Hello from the other side");

}
hello();

//Pre-arr0w function
const student = ["John", "Jane", "Judy"];

student.forEach(function(student){
	console.log(`${student} is a student`)
});

console.log("---------");
// forEach method with the use of arrow function
/*
	Syntax:

	arrayName.arrayMethod((parameter) =>
		//code to execute;
	)
*/
student.forEach((student) =>
	console.log(`${student} is a student`)
	);

// anonymous function - a function that has no function name

console.log("-----------------");
console.log("=> Implicit Return using arrow functions");

/*// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;
 */

/*
	Syntax:
	let/const variableName = (parameter/s) => code to execute;
*/

 const add = (x,y) => x + y;

 let total = add(1,2);
 console.log(total); // = 3

/* function add(x,y){
 	return x+y;

 };
 let total = add(1,2);
 console.log(total);*/


// [SECTION] Default Function Argument Value

// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning ${name}`;
console.log(greet());
console.log(greet("John"));

console.log("--------------")
console.log("=> Class-Based Object Blueprint: ");
// [SECTION] Class-Based Object Blueprint
// Another approach in creating an object with key and value;
// Allows creation/instantiation of object using classes as blueprints.

// = The "constructor" is a special method of a class for creating/initializing object for that class.

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// new operator
let myCar = new Car();
console.log(myCar);

// Reassigning value of each property

myCar = new Car("Ford", "Ranger Raptor", 2021);
console.log(myCar);


myCar.brand("Ford");
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);